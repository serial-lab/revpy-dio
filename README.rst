=========
RevPy_DIO
=========

This is a SIMPLE set of tools to monitor DIO inputs and to set outputs.  For
more complex monitoring of inputs, you can expand upon the examples and the
same is true for output manipulation as well.

If you just need to monitor inputs and control outputs one at a time, then
this package is for you.

Usage
=====

Input Monitoring
----------------

To monitor inputs, override the revpy_dio.inputs.InputMonitor class's
handle_input function.  As inputs are detected, this function will be called
along with the input number that was detected as being on.

To start the input monitor just call the run function and pass in a sleep interval
(in seconds) which will determine how often the inputs are checked:

.. code-block:: python

    from revpy_dio.inputs import InputMonitor

    class MyInputMonitor(InputMonitor):
        def handle_input(self, input_number: int):
        """
        When an input is activated, this function will be called along
        with the input number.  Override this to provide your functionality.
        """
        print('Input %s has been activated.' % input_number)

    if __name__ == '__main__':
        input = InputMonitor(sleep_interval=.15)
        input.run()



Controlling Outputs
-------------------
To set outputs on or off, use the set_output function:

.. code-block:: python

    from revpy_dio.outputs import set_output
    import time

    set_output(7, False) # turns off output 7
    set_output(6, True) # turns on output 6
    set_output(5, False) # turns off output 5
    set_output(15, True) # turns on output 15

    # to turn an output on for a specific amount of time and then turn off:
    set_output(1, True)
    time.sleep(.15)
    set_output(1, False)

    print('To check the individual output bits execute "piTest -r 70,2,b"')
