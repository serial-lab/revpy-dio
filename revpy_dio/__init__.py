# -*- coding: utf-8 -*-

"""Top-level package for revPy_dio."""

__author__ = """Rob Magee"""
__email__ = 'slab@serial-lab.com'
__version__ = '0.1.0'
