BIT_1 = 0b00000001
BIT_2 = 0b00000010
BIT_3 = 0b00000100
BIT_4 = 0b00001000
BIT_5 = 0b00010000
BIT_6 = 0b00100000
BIT_7 = 0b01000000
BIT_8 = 0b10000000


def get_bit_mask(output_id: int):
    ret = None
    if output_id == 1 or output_id == 9:
        ret = BIT_1
    if output_id == 2 or output_id == 10:
        ret = BIT_2
    if output_id == 3 or output_id == 11:
        ret = BIT_3
    if output_id == 4 or output_id == 12:
        ret = BIT_4
    if output_id == 5 or output_id == 13:
        ret = BIT_5
    if output_id == 6 or output_id == 14:
        ret = BIT_6
    if output_id == 7 or output_id == 15:
        ret = BIT_7
    if output_id == 8 or output_id == 16:
        ret = BIT_8
    return ret


def set_bit(bits, bit_mask, on=True):
    ret = bits | bit_mask if on else bits & bit_mask
    return ret


def get_off_mask(output_id: int):
    ret = get_bit_mask(output_id) ^ 0b11111111
    return ret


def set_output(output_id: int, on=True, left=True):
    """
    Turns an output on or off.
    :param output_id: The output to control.
    :param on: Turns the output on if true or off if false
    :return: None
    """
    offset = 70 if left else 81
    with open("/dev/piControl0", "wb+", 0) as revpi:
        bitmask = get_bit_mask(output_id) if on else get_off_mask(output_id)
        revpi.seek(offset)
        vals = revpi.read(2)
        revpi.seek(offset)
        if output_id < 1 or output_id > 16:
            raise IndexError('The output id must be between 1 and 16.')
        if output_id <= 8:
            byte_one = set_bit(vals[0], bitmask, on)
            byte_two = vals[1]
        else:
            byte_one = vals[0]
            byte_two = set_bit(vals[1], bitmask, on)
        revpi.write(bytearray([byte_one, byte_two]))


if __name__ == '__main__':
    set_output(7, False)
    set_output(6, True)
    set_output(5, False)
    set_output(15, True)
    print('To check the individual output bits execute "piTest -r 70,2,b"')
